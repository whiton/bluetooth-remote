package com.bluetooth.white.myapplication;

import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import app.akexorcist.bluetotohspp.library.BluetoothSPP;
import app.akexorcist.bluetotohspp.library.BluetoothState;

public class MainActivity extends AppCompatActivity {

    TextView textView, testInfo;
    Button[] buttons;
    Button btFire, btRight, btLeft, btConnect;
    BluetoothSPP bt = new BluetoothSPP(this);
    // It's stupid and totally unprofessional, I know, but I don't know how to 'listen' to the connection state using java package method
    boolean isConnected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);// 横屏
        setContentView(R.layout.activity_main);
        initView();
        initListener();
        for(int i = 0; i < 9; i++){
            buttons[i].setEnabled(false);
        }
        btFire.setEnabled(false);
        btLeft.setEnabled(false);
        btRight.setEnabled(false);
    }

    @Override
    public void onStart(){
        super.onStart();
        buttons[4].setVisibility(View.INVISIBLE);
        if(!bt.isBluetoothEnabled()) {
            textView.setText("蓝牙未打开");
        } else {
            if(!bt.isServiceAvailable()) {
                bt.setupService();
                textView.setText("蓝牙已打开");
            }
        }
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        bt.stopService();
    }

    private void initView(){
        btConnect = findViewById(R.id.bt_connect);
        textView = findViewById(R.id.tv_title);
        testInfo = findViewById(R.id.tv_test);
        buttons = new Button[9];
        buttons[0] = findViewById(R.id.btn1);
        buttons[1] = findViewById(R.id.btn2);
        buttons[2] = findViewById(R.id.btn3);
        buttons[3] = findViewById(R.id.btn4);
        buttons[4] = findViewById(R.id.btn5);
        buttons[5] = findViewById(R.id.btn6);
        buttons[6] = findViewById(R.id.btn7);
        buttons[7] = findViewById(R.id.btn8);
        buttons[8] = findViewById(R.id.btn9);
        btFire = findViewById(R.id.fire);
        btLeft = findViewById(R.id.bt_left);
        btRight = findViewById(R.id.bt_right);
    }


    private void initListener(){

        bt.setBluetoothConnectionListener(new BluetoothSPP.BluetoothConnectionListener() {
            public void onDeviceConnected(String name, String address) {
                // Do something when successfully connected
                isConnected = true;
                testInfo.setText("连接成功" + "  " + bt.getConnectedDeviceName() + " " + bt.getConnectedDeviceAddress());
                btConnect.setText("断 开");
                for(int i = 0; i < 9; i++){
                    buttons[i].setEnabled(true);
                }
                btFire.setEnabled(true);
                btLeft.setEnabled(true);
                btRight.setEnabled(true);
            }
            public void onDeviceDisconnected() {
                // Do something when connection was disconnected
                isConnected = false;
                testInfo.setText("连接中断");
                btConnect.setText("连 接");
                for(int i = 0; i < 9; i++){
                    buttons[i].setEnabled(false);
                }
                btFire.setEnabled(false);
                btLeft.setEnabled(false);
                btRight.setEnabled(false);
            }
            public void onDeviceConnectionFailed() {
                // Do something when connection failed
                isConnected = false;
                testInfo.setText("连接中断");
                btConnect.setText("连 接");
                for(int i = 0; i < 9; i++){
                    buttons[i].setEnabled(false);
                }
                btFire.setEnabled(false);
                btLeft.setEnabled(false);
                btRight.setEnabled(false);
            }
        });

        btConnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(bt.isServiceAvailable()) {
                    bt.startService(BluetoothState.DEVICE_OTHER);
                    bt.connect("98:D3:35:71:09:15");
                    if(!isConnected) {
                        testInfo.setText("连接中...");
                    }
                    //bt.connect("AC:10:5A:A6:11:DB");
                    //btConnect.setActivated(false);
                }
            }
        });

        buttons[0].setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_UP://松开事件发生后执行代码的区域
                        textView.setText("停止");
                        bt.send("m8", false);
                        break;
                    case MotionEvent.ACTION_DOWN://按住事件发生后执行代码的区域
                        textView.setText("↖");
                        bt.send("m4", false);
                        break;
                    default:
                        break;

                }
                return true;
            }
        });

        buttons[1].setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_UP://松开事件发生后执行代码的区域
                        textView.setText("停止");
                        bt.send("m8", false);
                        break;
                    case MotionEvent.ACTION_DOWN://按住事件发生后执行代码的区域
                        textView.setText("↑");
                        bt.send("m0", false);
                        break;
                    default:
                        break;

                }
                return true;
            }
        });


        buttons[2].setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_UP://松开事件发生后执行代码的区域
                        textView.setText("停止");
                        bt.send("m8", false);
                        break;
                    case MotionEvent.ACTION_DOWN://按住事件发生后执行代码的区域
                        textView.setText("↗");
                        bt.send("m5", false);
                        break;
                    default:
                        break;

                }
                return true;
            }
        });

        buttons[3].setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_UP://松开事件发生后执行代码的区域
                        textView.setText("停止");
                        bt.send("m8", false);
                        break;
                    case MotionEvent.ACTION_DOWN://按住事件发生后执行代码的区域
                        textView.setText("←");
                        bt.send("m2", false);
                        break;
                    default:
                        break;

                }
                return true;
            }
        });

        buttons[4].setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_UP://松开事件发生后执行代码的区域
                        break;
                    case MotionEvent.ACTION_DOWN://按住事件发生后执行代码的区域
                        break;
                    default:
                        break;

                }
                return true;
            }
        });

        buttons[5].setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_UP://松开事件发生后执行代码的区域
                        textView.setText("停止");
                        bt.send("m8", false);
                        break;
                    case MotionEvent.ACTION_DOWN://按住事件发生后执行代码的区域
                        textView.setText("→");
                        bt.send("m3",false);
                        break;
                    default:
                        break;

                }
                return true;
            }
        });

        buttons[6].setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_UP://松开事件发生后执行代码的区域
                        textView.setText("停止");
                        bt.send("m8", false);
                        break;
                    case MotionEvent.ACTION_DOWN://按住事件发生后执行代码的区域
                        textView.setText("↙");
                        bt.send("m6", false);
                        break;
                    default:
                        break;

                }
                return true;
            }
        });

        buttons[7].setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_UP://松开事件发生后执行代码的区域
                        textView.setText("停止");
                        bt.send("m8", false);
                        break;
                    case MotionEvent.ACTION_DOWN://按住事件发生后执行代码的区域
                        textView.setText("↓");
                        bt.send("m1", false);
                        break;
                    default:
                        break;

                }
                return true;
            }
        });

        buttons[8].setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_UP://松开事件发生后执行代码的区域
                        textView.setText("停止");
                        bt.send("m8", false);
                        break;
                    case MotionEvent.ACTION_DOWN://按住事件发生后执行代码的区域
                        textView.setText("↘");
                        bt.send("m7", false);
                        break;
                    default:
                        break;

                }
                return true;
            }
        });

        btFire.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_UP://松开事件发生后执行代码的区域
                        textView.setText("停止");
                        bt.send("p0", false);
                        break;
                    case MotionEvent.ACTION_DOWN://按住事件发生后执行代码的区域
                        textView.setText("开火！");
                        bt.send("p1", false);
                        break;
                    default:
                        break;

                }
                return true;
            }
        });

        btLeft.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_UP://松开事件发生后执行代码的区域
                        textView.setText("停止");
                        bt.send("s2", false);
                        break;
                    case MotionEvent.ACTION_DOWN://按住事件发生后执行代码的区域
                        textView.setText("左转");
                        bt.send("s0", false);
                        break;
                    default:
                        break;

                }
                return true;
            }
        });

        btRight.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_UP://松开事件发生后执行代码的区域
                        textView.setText("停止");
                        bt.send("s2", false);
                        break;
                    case MotionEvent.ACTION_DOWN://按住事件发生后执行代码的区域
                        textView.setText("右转");
                        bt.send("s1", false);
                        break;
                    default:
                        break;

                }
                return true;
            }
        });

    }

}
